import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App';

function configureReact() {
  const rootDomContainer = document.querySelector('#react-root-dom')
  ReactDOM.render(React.createElement(App), rootDomContainer)
}

export default configureReact