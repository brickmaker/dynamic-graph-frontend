import React, { Component } from 'react'
import HorizontalScroll from 'react-scroll-horizontal'
import { updateTimeSelection } from '../../main-graph'
import * as timeInfoData from '../../../data/college-msg/timeInfo.json'
const timeInfoArr = timeInfoData.map((d) => d)

class Minimap extends Component {
  selectImage(timestamp) {
    const startTime = new Date(parseInt(timestamp))
    const endTime = new Date(startTime.getTime() + 1)
    updateTimeSelection(startTime, endTime)
  }

  render() {
    const parent = {
      width: `1200px`,
      height: `200px`,
      border: 'solid 2px'
    }
    return (
      <div style={parent}>
        <HorizontalScroll>
          {
            timeInfoArr.map((t) => {
              const timestamp = t.timestamp
              return <Cell
                key={timestamp}
                timestamp={timestamp}
                onClick={() => {
                  this.selectImage(timestamp)
                }} />
            })
          }
        </HorizontalScroll>
      </div>
    )
  }
}

class Cell extends Component {
  render() {
    const filename = `./minimaps/college-msg-minimap/${this.props.timestamp}.png`
    const date = new Date(parseInt(this.props.timestamp))
    const dateStr = date.toLocaleDateString()
    return (
      <div onClick={this.props.onClick}>
        <div style={{
          border: 'solid 1px',
          margin: 5
        }}>
          <img width='200' height='auto' src={filename} />
        </div>
        <p style={{
          margin: 5,
          textAlign: "center"
        }}>{dateStr}</p>
      </div>
    )
  }
}

export default Minimap