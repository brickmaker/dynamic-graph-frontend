import React, { Component } from 'react'
import OperationPanel from './OperationPanel';
import Minimap from './Minimap';

class App extends Component {
  render() {
    return (
      <div>
        <Minimap />
      </div>
    )
  }
}

export default App