import * as PIXI from 'pixi.js'
import * as Viewport from 'pixi-viewport'
// import * as linksData from '../data/linksData.json'
// import * as nodesData from '../data/nodesData.json'
// import * as timeInfoData from '../data/timeInfo.json'
const TEST_ALL = false
import { MAIN_CANVAS } from './constants.js'
import * as linksData from '../data/college-msg/linksData.json'
import * as nodesData from '../data/college-msg/nodesData.json'
import * as timeInfoData from '../data/college-msg/timeInfo.json'
import { getMonthDate } from './helper/date-tool.js';

const timeInfoArr = timeInfoData.map((d) => d)
// -- state
let startTime = new Date(2000, 1, 1)
let endTime = new Date(2020, 1, 1)

const pixiApp = new PIXI.Application({
  width: MAIN_CANVAS.width,
  height: MAIN_CANVAS.height
})

const pixiViewport = new Viewport({
  screenWidth: MAIN_CANVAS.width,
  screenHeight: MAIN_CANVAS.height,
  worldWidth: MAIN_CANVAS.width * 10,
  worldHeight: MAIN_CANVAS.height * 10,

  interaction: pixiApp.renderer.interaction
})

const nodesSet = new Set()
const linksSet = new Set()
export function initPixiApp() {
  document.getElementById('main-container').appendChild(pixiApp.view)
  const canvas = document.getElementById('main-container').getElementsByTagName('canvas')[0]
  // canvas.setAttribute('style', 'position: absolute; left: 0; top: 0; z-index: -1') TODO: comment it and move iteraction done
  pixiApp.renderer.backgroundColor = 0xeeeeee

  pixiApp.stage.addChild(pixiViewport)
  pixiViewport
    .drag()
    .pinch()
    .wheel()
    .decelerate()

  draw()
}

function clearStage() { // TODO: 就没有一个清理Stage的API嘛QAQ
  while (pixiViewport.children[0]) {
    pixiViewport.removeChild(pixiViewport.children[0])
  }
  // while (pixiApp.stage.children[0]) {
  // pixiApp.stage.removeChild(pixiApp.stage.children[0])
  // }
}

function computeValidSet(t1, t2) {
  nodesSet.clear()
  linksSet.clear()

  if (TEST_ALL) {
    nodesData.map(node => {
      nodesSet.add(node.id)
    })
    linksData.map(link => {
      linksSet.add(link.id)
    })
  } else {
    timeInfoArr.filter(term => {
      const t = new Date(parseInt(term.timestamp))
      return t >= t1 && t < t2
    }).map(term => {
      term.nodes.map(node => {
        nodesSet.add(node)
      })
      term.links.map(link => {
        linksSet.add(link)
      })
    })
  }
}

function draw() {
  computeValidSet(startTime, endTime)

  const NODE_RADIUS = 2
  const LINE_THICKNESS = 0.7

  linksData
    .filter((link) => {
      return linksSet.has(link.id)
    })
    .map(link => {
      let line = new PIXI.Graphics();
      line.lineStyle(LINE_THICKNESS, 0x888888, 0.7)
      line.moveTo(link.source.x, link.source.y)
      line.lineTo(link.target.x, link.target.y)
      // pixiApp.stage.addChild(line);
      pixiViewport.addChild(line);
    })

  nodesData
    .filter((node) => {
      return nodesSet.has(node.id)
    })
    .map(node => {
      let circle = new PIXI.Graphics(); // may be optimized
      circle.lineStyle(0.5, 0xe0e0e0, 0.5);  //(thickness, color)
      circle.beginFill(0x666666);
      circle.drawCircle(node.x, node.y, NODE_RADIUS);
      circle.endFill();
      // pixiApp.stage.addChild(circle);
      pixiViewport.addChild(circle);
    })
}

function updateTimeSelection(t1, t2) {
  startTime = t1
  endTime = t2
  clearStage()
  draw()
}

function generatePivotTimeMinimap() {
  for (const t of timeInfoArr) {
    const startTime = getMonthDate(new Date(parseInt(t.timestamp)))
    const endTime = new Date(startTime.getTime() + 1)

    setTimeout(() => {
      console.log(startTime)
      console.log(endTime)

      updateTimeSelection(startTime, endTime)
    }, 2000);
  }
}

export { updateTimeSelection, generatePivotTimeMinimap }