export default (polygon, point) => {
  let flag = false;
  let i = 0, j = polygon.length - 1
  while (i < polygon.length) {
    const pa = polygon[i]
    const pb = polygon[j]
    const intersect =
      ((pa.y > point.y) !== (pb.y > point.y)) &&
      (point.x < pa.x + (pb.x - pa.x) * (point.y - pa.y) / (pb.y - pa.y))
    if (intersect)
      flag = !flag
    j = i
    i += 1
  }
  return flag
}