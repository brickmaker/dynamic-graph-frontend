import { initPixiApp } from './main-graph'
import { drawTimeline } from './timeline'
// import { initLassoSelection } from './lasso-selection'
import configureReact from './react/entry';


// - execution

initPixiApp()
drawTimeline()
// initLassoSelection()
configureReact()