import * as d3 from 'd3'
// import { lasso } from 'd3-lasso'
import lasso from './helper/lasso.js'
import { MAIN_CANVAS } from './constants.js'
// import * as nodesData from '../data/nodesData.json'
import * as nodesData from '../data/sample-5points/nodesData.json'

function initLassoSelection() {
  const lassoSVG = d3.select('#main-container').append("svg")
  lassoSVG
    .attr('position', 'absolute')
    .attr('top', 0)
    .attr('left', 0)
    .attr('width', MAIN_CANVAS.width)
    .attr('height', MAIN_CANVAS.height)

  const nodesArray = nodesData.map(d => d) // TODO: 奇怪的处理
  const lassoTool = lasso();
  lassoTool
    .closePathSelect(true)
    .closePathDistance(300)
    // .items(circles)
    .items(nodesArray)
    .targetArea(lassoSVG)
    .on("start", lassoStart)
    .on("draw", lassoDraw)
    .on("end", lassoEnd)

  function lassoStart() {

  }

  function lassoDraw() {

  }

  function lassoEnd() {
    const items = lassoTool.selectedItems()
    console.log(items)
  }

  lassoSVG.call(lassoTool)
}

export { initLassoSelection }
